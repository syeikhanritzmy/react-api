import { useState, useEffect } from 'react';
import axios from 'axios';

const API = import.meta.env.VITE_API_CURRENCY;

function ReactApi() {
  const [datas, setData] = useState([]);

  useEffect(() => {
    getApi();
  }, []);
  let number = 1;

  const getApi = async () => {
    try {
      let data = await axios.get(
        `https://api.currencyfreaks.com/latest?apikey=${API}&symbols=CAD,EUR,IDR,JPY,CHF,GBP,USD`
      );
      setData(data.data);
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <div className="container mx-auto flex flex-col justify-center w-full items-center">
        <table className="table-auto border-collapse border border-slate-400 mx-auto mt-56 ">
          <thead>
            <tr>
              <th className="text-center font-medium  ">Base : {datas.base} </th>
            </tr>
            <tr>
              <th className="py-3 px-4 border border-slate-300">No</th>
              <th className="py-3 px-4 border border-slate-300">Currency</th>
              <th className="py-3 px-4 border border-slate-300">We Buy</th>
              <th className="py-3 px-4 border border-slate-300">
                Exchange Rate
              </th>
              <th className="py-3 px-4 border border-slate-300">We Sell</th>
            </tr>
          </thead>
          <tbody>
            {datas.rates &&
              Object.keys(datas.rates).map((data, index) => (
                <tr key={index}>
                  <td className="text-center border border-slate-300">
                    {number++}
                  </td>
                  <td className="text-center border border-slate-300">
                    {data}
                  </td>
                  <td className="text-center border border-slate-300">
                    {(
                      parseFloat(datas.rates[data]) +
                      datas.rates[data] * 0.05
                    ).toFixed(6)}
                  </td>
                  <td className="text-center border border-slate-300">
                    {parseFloat(datas.rates[data]).toFixed(6)}
                  </td>
                  <td className="text-center border border-slate-300">
                    {' '}
                    {(
                      parseFloat(datas.rates[data]) -
                      datas.rates[data] * 0.05
                    ).toFixed(6)}
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default ReactApi;
