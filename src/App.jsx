import ReactApi from './components/ReactApi';

function App() {
  return (
    <div className="App">
      <ReactApi />
    </div>
  );
}

export default App;
